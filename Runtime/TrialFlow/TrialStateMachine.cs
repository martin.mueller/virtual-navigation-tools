﻿namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class provides the basic implementation of a trial state machine.
    /// New state machines should derive from this class.
    /// </summary>
    public class TrialStateMachine
    {
        /// <summary>
        /// The current state of the state machine.
        /// </summary>
        public ITrialState CurrentState { get; private set; }

        /// <summary>
        /// The default state in which the state machine is initialised.
        /// </summary>
        public static Empty Empty;

        /// <summary>
        /// Static constructor, which creates the empty state for the state machine.
        /// This is invoced automatically and cannot be called directly.
        /// </summary>
        static TrialStateMachine()
        {
            Empty = new Empty();
        }

        /// <summary>
        /// Main constructor, which initialises the state machine in its default, Empty state.
        /// </summary>
        public TrialStateMachine()
        {
            CurrentState = Empty;
        }

        /// <summary>
        /// This tells the state machine to switch to the given state.
        /// </summary>
        /// <param name="newState"> the new state that the machine should enter</param>
        public void SetState(ITrialState newState)
        {
            CurrentState.Exit();
            CurrentState = newState;
            CurrentState.Enter(this);
        }
    }
}
