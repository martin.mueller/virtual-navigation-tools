﻿using System;
using System.Collections;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This is the base class for any trial state handler.
    /// It defines the minimal required functionality 
    /// to work with the TrialStates of a TrialStateMachine.
    /// We can define a specific handler class, derived from this base class,
    /// to manage all the behaviour we want to associate with a given trial state.
    /// </summary>
    public abstract class TrialStateHandler : MonoBehaviour
    {
        /// <summary>
        /// A reference to the state we want to handle.
        /// </summary>
        public abstract TrialState HandledState { get; }

        /// <summary>
        /// Here we subscribe to the events broadcasted by the state we are handling.
        /// </summary>
        protected virtual void SubscribeToStateEvents()
        {
            TrialState.OnEnter += HandleEnter;
            TrialState.OnExit += HandleExit;
        }

        /// <summary>
        /// Here we unsubscribe from the events broadcasted by the state we are handling.
        /// </summary>
        protected virtual void UnsubscribeFromStateEvents()
        {
             TrialState.OnEnter -= HandleEnter;
             TrialState.OnExit -= HandleExit;
        }
        /// <summary>
        /// Here we define what should happen when the statemachine enters the state we are handling.
        /// </summary>
        /// <param name="sender"> the object firing the event</param>
        /// <param name="stateMachine"> the state machine we are working with</param>
        protected abstract void HandleEnter(object sender, TrialStateMachine stateMachine);

        /// <summary>
        /// Here we define what should happen when the statemachine leaves the state we are handling.
        /// </summary>
        /// <param name="sender"> the object firing the event</param>
        /// <param name="stateMachine"> the state machine we are working with</param>
        protected abstract void HandleExit(object sender, EventArgs e);

        /// <summary>
        /// Here we define what should happen while the state machine is in the handled state.
        /// This method is usually called in HandeEnter().
        /// Because this method runs as a coroutine, we can pause its execution to wait for an external event
        /// without interrupting the flow of the rest of our program.
        /// </summary>
        /// <returns></returns>
        protected abstract IEnumerator RunStateLogic();

        /// <summary>
        /// Here we tell the state machine which new state to transition to.
        /// This method is usually called at the end of RunStateLogic().
        /// </summary>
        /// <param name="stateMachine"> the state machine we are working with</param>
        /// <param name="state"> the new state we want to transition the state machine to</param>
        protected void SetNextTrialState(TrialStateMachine stateMachine, ITrialState state)
        {
            stateMachine.SetState(state);
        }

        /// <summary>
        /// When the state handler is created,
        /// we immediately subscribe to the events broadcasted by the state we want to handle.
        /// </summary>
        protected virtual void Start() { SubscribeToStateEvents(); }

        /// <summary>
        /// When the handler is destroyed, we unsubscribe to prevent memory leaks.
        /// </summary>
        protected virtual void OnDestroy() { UnsubscribeFromStateEvents(); }
    }
}

