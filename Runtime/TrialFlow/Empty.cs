﻿namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This state is used as the default state of the sate machine,
    /// nothing happens while the machine is in this state.
    /// </summary>
    public class Empty : TrialState
    {
        public Empty() : base("Empty"){ }

        public override void Enter(TrialStateMachine stateMachine) { }

        public override void Exit() { }
    }
}