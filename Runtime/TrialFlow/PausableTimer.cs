﻿using System;
using UnityEngine;


namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class provides a pausable timer, which can be managed either via script or via the Unity Editor.
    /// </summary>
    public class PausableTimer : MonoBehaviour
    {
        #region exposed fields for editor control
        [SerializeField] private string id = "DefaultTimer";
        [SerializeField] private double timeoutInSeconds = 999;
        [SerializeField] private bool isPaused = true;
        [SerializeField] private bool isActive = false;
        [SerializeField] private bool outputDebugMessages = false;
        #endregion

        #region events
        public static event EventHandler HasStarted;
        public static event EventHandler HasPaused;
        public static event EventHandler HasStopped;
        #endregion

        #region Properties
        /// <summary>
        /// The identifier of the timer instance.
        /// </summary>
        public string ID { get => id; set { id = value; } }
        /// <summary>
        /// The value indicating how many seconds the timer has run.
        /// </summary>
        public double CurrentValue { get; private set; }
        /// <summary>
        /// The value indicating when the timer wghile run out and stop.
        /// </summary>
        public double TimeoutInSeconds { get => timeoutInSeconds; }
        /// <summary>
        /// A flag showing whether the timer has run out yet.
        /// </summary>
        public bool HasReachedTimeout { get => (CurrentValue >= TimeoutInSeconds); }
        /// <summary>
        /// A flag showing whether the timer is currently running (i.e not inactive or paused).
        /// </summary>
        public bool IsRunning { get => (!isPaused && isActive); }
        #endregion

        private void Update()
        {
            StopTimerIfTimeoutIsReached();
            RunTimerIfAllowed();
            if (outputDebugMessages)
            {
                Debug.LogFormat("current timer value is: {0}," +
                                " timer is paused: {1}," +
                                " timer is active: {2}",
                                CurrentValue, isPaused, isActive);
            }
        }

        /// <summary>
        /// Here we check if the timer has reached the timeout value, 
        /// and if it has, we stop the timer.
        /// </summary>
        private void StopTimerIfTimeoutIsReached()
        {
            if (HasReachedTimeout)
            {
                isActive = false;
                if (outputDebugMessages) { Debug.LogFormat("timeout reached at: {0}", CurrentValue); }
                HasStopped?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Here we check if the timer is currently running, 
        /// and if it is, we advance the current timer value.
        /// </summary>
        private void RunTimerIfAllowed()
        {
            if (IsRunning)
            {
                CurrentValue += Time.deltaTime;
            }
        }

        /// <summary>
        /// Here we can set a new timeout value, event if the timer is already running.
        /// This can be used for example to grant players additional time for a task.
        /// </summary>
        /// <param name="timeoutInSeconds"></param>
        public void SetTimeoutValue(double timeoutInSeconds)
        {
            if (isActive) { Debug.LogWarning("Overwriting timeout while timer is running. This may lead to unexpected behaviour!"); }
            this.timeoutInSeconds = timeoutInSeconds;
        }

        /// <summary>
        /// This is the main method to start a new timer.
        /// </summary>
        /// <param name="timeoutInSeconds"></param>
        public void StartWithNewTimeout(double timeoutInSeconds)
        {
            if (isActive)
            {
                Debug.LogWarning("Timer already active. Ignoring StartWithNewTimeout() prompt.");
            }
            else if (!isActive)
            {
                isActive = true;
                isPaused = false;
                CurrentValue = 0f;
                this.timeoutInSeconds = timeoutInSeconds;
                if (outputDebugMessages) { Debug.LogFormat("Starting new timer. Timer will run for {0} seconds", this.timeoutInSeconds); }
                HasStarted?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Here we can pause the currently running timer.
        /// Does nothing if timer is not running or already paused.
        /// </summary>
        public void Pause()
        {
            if (!isActive) { Debug.LogWarning("Timer not running, nothing to pause."); }
            else if (isActive)
            {
                if (!isPaused)
                {
                    if (outputDebugMessages) { Debug.LogFormat("Pausing timer at {0}", CurrentValue); }
                    isPaused = true;
                    HasPaused?.Invoke(this, EventArgs.Empty);
                }
                else { Debug.LogWarning("Timer already paused!"); }
            }
        }

        /// <summary>
        /// Here we can resume the timer, after it has been paused.
        /// Does nothing if timer is not paused.
        /// </summary>
        public void ResumeTimer()
        {
            if (!isActive) { Debug.LogWarning("Timer not running, nothing to resume."); }
            else if (isActive)
            {
                if (isPaused)
                {
                    if (outputDebugMessages) { Debug.LogFormat("Resuming timer at {0}", CurrentValue); }
                    isPaused = false;
                }
                else { Debug.LogWarning("Timer already running, no need to resume."); }
            }
        }

        /// <summary>
        /// This stops and resets the timer to zero.
        /// Warning: Erases previous timer value, cannot be undone!
        /// </summary>
        public void StopAndReset()
        {
            if (outputDebugMessages) { Debug.Log("Stopping and Resetting timer."); }
            CurrentValue = 0f;
            isActive = false;
        }
    }
}





